import React from 'react';
import PageContainer from './containers/PageContainer.js'
import './App.css';

function App() {
  return (
    <div className="App">
      <PageContainer />
    </div>
  );
}

export default App;
