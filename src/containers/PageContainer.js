import React, { useState, useEffect } from "react";
import ReactWaves from "@dschoon/react-waves";
import { Container, Row, Col } from "reactstrap";

import ControlBar from "../components/ControlBar";
import SearchInput from "../components/SearchInput";
import TranscribeBox from "../components/TranscribeBox";
import ShareIcon from "../assets/share.svg";
import Audio from "../assets/testexam.wav";
import Transcription from '../assets/transcript.json'
import "./PageContainer.css";

function PageContainer() {
  const [isPlaying, setIsPlaying] = useState(false);
  const [currentPos , setCurrentPos] = useState(0);
  const [transcript, setTranscript] = useState({
    transcript_text: [],
    word_timings: []
  });

  const handlePausePlay = () => {
    setIsPlaying(!isPlaying);
  };

  const handleClickWord = (word) => {
    setCurrentPos(0.5)
  }

  const getTimeFormat = (time) => {
    const withoutS = time.substring(0, time.length - 1);
    const rawFormat = withoutS.split(".")
    const seconds = rawFormat[0].length <= 1 ? `0${rawFormat[0]}` : rawFormat[0];

    if(rawFormat.length === 1) {
      rawFormat.push("00")
    }

    const ms = rawFormat[1].length <= 1 ? "00" : rawFormat[1].substring(0, 2);
    return `${seconds}:${ms}`;
  }

  const getTimeValue = (time) => {
    const withoutS = time.substring(0, time.length - 1);
    return Number(withoutS).toFixed(2)
  }

  // organize transcript record first.
  useEffect(() => {
    const deepClone = {...Transcription};
    deepClone.word_timings.map((words) => {
      return words.map((word) => {
        word.active = false;
        word.startTimeVal = getTimeValue(word.startTime)
        word.startTime = getTimeFormat(word.startTime);
        word.endTimeVal = getTimeValue(word.endTime)
        word.endTime = getTimeFormat(word.endTime);
        return word;
      });
    });
    setTranscript(deepClone)
  }, [])

  useEffect(() => {
    transcript.word_timings.map(sentence => {
      return sentence.map(word => {
        if(word.startTimeVal === currentPos) {
          word.active = true;
        }
        if(word.endTimeVal < currentPos) {
          word.active = false;
        }
      })
    })
  }, [currentPos])

  return (
    <Container fluid>
      <Row className="control-header">
        <Col className="control-col left">
          <ControlBar handlePausePlay={handlePausePlay} isPlaying={isPlaying} />
        </Col>
        <Col className="control-col right">
          <button className="share-button">
            <img src={ShareIcon} alt="share-icon" />
            Share
          </button>
        </Col>
      </Row>
      <Row>
        <ReactWaves
          pos={currentPos}
          audioFile={Audio}
          className={"react-waves"}
          options={{
            barGap: 1,
            barWidth: .8,
            barHeight: 2,
            cursorWidth: 1,
            height: 150,
            minHeight: 150,
            hideScrollbar: true,
            progressColor: '#EC407A',
            normalize: true,
            responsive: true,
            waveColor: '#D1D6DA',
          }}
          volume={1}
          zoom={1}
          playing={isPlaying}
          onPosChange={(pos, wavesurfer) => { 
            setCurrentPos(pos.toFixed(2)) }}
        />
      </Row>
      <Row className="row-p-15">
        <SearchInput />
      </Row>
      <Row className="row-p-15 row-flex-column">
        <TranscribeBox handleClickWord={handleClickWord} currentPos={currentPos} transcript={transcript}/>
      </Row>
    </Container>
  );
}

export default PageContainer;
