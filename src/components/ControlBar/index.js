import React from "react";
import RotateLeft from "../../assets/rotate-left.svg";
import RotateRight from "../../assets/rotate-right.svg";
import PlayButton from "../../assets/play-circle-fill.svg";
import PauseButton from "../../assets/pause-circle-fill.svg";
import "./styles.css";

function ControlBar(props) {
  const { handlePausePlay, isPlaying } = props;

  const renderPlayPauseButton = () => {
    return (
      <img
        onClick={handlePausePlay}
        className="pause-circle-fill"
        src={isPlaying ? PauseButton : PlayButton}
        alt="pause-button"
      />
    );
  };

  return (
    <div className="control-bar">
      <img
        className="rotate-button left-button"
        src={RotateLeft}
        alt="rotate-left-button"
      />
      {renderPlayPauseButton()}
      <img
        className="rotate-button right-button"
        src={RotateRight}
        alt="rotate-right-button"
      />
      <span className="oval">1.0x</span>
    </div>
  );
}

export default ControlBar;
