import React from "react";
import "./styles.css";

export default function TranscribeBox(props) {
  const { isAgent, transcript, handleClickWord } = props;
  const userType = isAgent ? 'agent' : 'client'
  
  const getStartTime = (index) => {
    const startTime = transcript.word_timings[index][0].startTime
    return startTime;
  }

  const renderWord = (items) => {
    return items.map((item) => {
      const active = item.active ? "selected-word" : ""
      return <span onClick={() => {handleClickWord(item)}} className={`word ${active}`}>{` ${item.word}`}</span>;
    });
  };

  const renderScript = () => {
    return transcript.word_timings.map((words, index) => {
      return (
        <div className="transcribe-box-container" key={`transcribe-${index}`}>
          <div className={`time ${userType}`}>{getStartTime(index)}</div>
          <div className={`message-box ${userType}`}>{renderWord(words)}</div>
        </div>
      );
    })
  }

  return (
    <>
      {renderScript()}
    </>
  );
}
