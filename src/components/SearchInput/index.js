import React from "react";
import "./styles.css";

function SearchInput() {
  return (
    <div>
      <input className="input-base" placeholder="Search call transcript" />
    </div>
  );
}

export default SearchInput;
